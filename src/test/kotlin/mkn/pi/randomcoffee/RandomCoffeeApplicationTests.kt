package mkn.pi.randomcoffee

import mkn.pi.randomcoffee.controller.MatchController
import mkn.pi.randomcoffee.controller.OrganizationController
import mkn.pi.randomcoffee.controller.UserController
import mkn.pi.randomcoffee.dto.OrganizationDto
import mkn.pi.randomcoffee.dto.ParticipationAnswerDto
import mkn.pi.randomcoffee.dto.RegistrationUserDto
import mkn.pi.randomcoffee.dto.UserDto
import mkn.pi.randomcoffee.repository.UserRepository
import mkn.pi.randomcoffee.scheduler.ScheduleService
import org.junit.jupiter.api.*
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.SpyBean
import org.springframework.test.context.TestPropertySource
import java.lang.Thread.sleep

@SpringBootTest
class RandomCoffeeApplicationTests(
    @Autowired private val organizationController: OrganizationController,
    @Autowired private val userController: UserController,
    @Autowired private val matchController: MatchController,
    @Autowired private val userRepository: UserRepository
) {

    @SpyBean
    private lateinit var scheduleService: ScheduleService

    @Test
    @Suppress("EmptyFunctionBlock")
    fun contextLoads() {
    }

    @BeforeEach
    fun clear() {
        userRepository.deleteAll()
        organizationController.organizationComponent.organizationRepository.deleteAll()
    }

    @Test
    fun `User agreed with match but then changed his mind, there is no match with this user`() {
        val organizationDto = OrganizationDto(null, "MCS", "MCS", "1234")
        organizationController.addOrganization(organizationDto)

        val user1 = RegistrationUserDto(1, "Lana", "Lana", "Feeling good", "MCS", "1234")
        val user2 = RegistrationUserDto(2, "Denis", "Denis", "Young man", "MCS", "1234")
        userController.registerUser(user1)
        userController.registerUser(user2)

        userController.updateParticipationConfirm(1)
        userController.updateParticipationConfirm(2)

        userController.updateParticipationAnswer(1, ParticipationAnswerDto(true))
        userController.updateParticipationAnswer(1, ParticipationAnswerDto(false))
        userController.updateParticipationAnswer(2, ParticipationAnswerDto(true))
        scheduleService.doMatching()
        Assertions.assertTrue(matchController.getMatchingsNeedNotification().isEmpty())
    }

    @Test
    fun `If even number of participants, everyone receive a match`() {
        Mockito.`when`(scheduleService.matching()).then {  }
        val organizationDto = OrganizationDto(null, "MCS", "MCS", "1234")
        organizationController.addOrganization(organizationDto)

        for (i in 1L..100L) {
            userController.registerUser(RegistrationUserDto(i, "$i", "$i", "", "MCS", "1234"))
            userController.updateParticipationConfirm(i)
            userController.updateParticipationAnswer(i, ParticipationAnswerDto(true))
        }

        scheduleService.doMatching()
        val matches = matchController.getMatchingsNeedNotification()
        Assertions.assertEquals(50, matches.size)

        val matchedIds = mutableSetOf<Long>()
        matches.forEach {
            matchedIds.add(it.user1.id)
            matchedIds.add(it.user2.id)
        }

        Assertions.assertEquals(100, matchedIds.size)
    }

    @Test
    fun `If one of the matched users cancels match, his partner is notified`() {
        val organizationDto = OrganizationDto(null, "MCS", "MCS", "1234")
        organizationController.addOrganization(organizationDto)

        Mockito.`when`(scheduleService.matching()).then {  }
        val user1 = RegistrationUserDto(1, "Lana", "Lana", "Feeling good", "MCS", "1234")
        val user2 = RegistrationUserDto(2, "Marina", "Marina", "Love corgis", "MCS", "1234")
        userController.registerUser(user1)
        userController.registerUser(user2)

        userController.updateParticipationConfirm(1)
        userController.updateParticipationConfirm(2)

        userController.updateParticipationAnswer(1, ParticipationAnswerDto(true))
        userController.updateParticipationAnswer(2, ParticipationAnswerDto(true))

        scheduleService.doMatching()

        userController.userCancelMatch(1)

        val usersWithCancelledMatches = userController.usersWithCancelledMatches()
        Assertions.assertEquals(1, usersWithCancelledMatches.size)
        Assertions.assertEquals(2, usersWithCancelledMatches[0].id)
    }
}
