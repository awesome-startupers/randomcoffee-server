package mkn.pi.randomcoffee.repository

import mkn.pi.randomcoffee.entity.OrganizationEntity
import org.springframework.data.repository.CrudRepository
import java.util.*

interface OrganizationRepository : CrudRepository<OrganizationEntity, Long> {
    fun findByKeyword(keyword: String): Optional<OrganizationEntity>
}
