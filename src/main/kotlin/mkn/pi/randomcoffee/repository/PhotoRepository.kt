package mkn.pi.randomcoffee.repository

import mkn.pi.randomcoffee.entity.user.PhotoEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Repository
@Transactional
interface PhotoRepository: JpaRepository<PhotoEntity, Long> {
    fun findByUserId(userId: Long): Optional<PhotoEntity>
}
