package mkn.pi.randomcoffee.repository

import mkn.pi.randomcoffee.entity.MatchEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.Optional

@Repository
@Transactional
interface MatchRepository: CrudRepository<MatchEntity, Long> {
    fun findByFirstUserIdOrSecondUserId(firstUserId: Long, secondUserId: Long): Optional<MatchEntity>
    fun deleteByFirstUserId(usedId: Long)
}
