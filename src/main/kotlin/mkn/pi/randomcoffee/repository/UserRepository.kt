package mkn.pi.randomcoffee.repository

import mkn.pi.randomcoffee.entity.user.UserEntity
import mkn.pi.randomcoffee.entity.user.UserState
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<UserEntity, Long> {
    fun findAllByNeedConfirmation(needConfirmation: Boolean): List<UserEntity>
    fun findAllByState(userState: UserState): List<UserEntity>
}
