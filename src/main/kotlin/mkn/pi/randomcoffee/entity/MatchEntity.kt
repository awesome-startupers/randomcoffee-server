@file:Suppress("WildcardImport")
package mkn.pi.randomcoffee.entity

import javax.persistence.*

@Entity(name = "matches")
@SequenceGenerator(allocationSize = 1, name = "match_seq", sequenceName = "match_seq")
data class MatchEntity (
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "match_seq") val id: Long? = null,
    val firstUserId: Long,
    val secondUserId: Long
)
