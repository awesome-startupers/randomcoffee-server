package mkn.pi.randomcoffee.entity

import org.hibernate.Hibernate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.GenerationType

@Entity(name = "organizations")
@SequenceGenerator(allocationSize = 1, name = "match_seq", sequenceName = "match_seq")
data class OrganizationEntity(
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "match_seq") val id: Long? = null,
    val name: String,
    val keyword: String,
    val pincode: String,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as OrganizationEntity

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id )"
    }
}
