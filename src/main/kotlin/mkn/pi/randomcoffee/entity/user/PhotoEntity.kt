package mkn.pi.randomcoffee.entity.user

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Lob
import javax.persistence.SequenceGenerator


@Entity(name = "photos")
@SequenceGenerator(allocationSize = 1, name = "photo_seq", sequenceName = "photo_seq")
data class PhotoEntity(

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "photo_seq") val id: Long? = null,
    val userId: Long,

    @Lob
    val rawData: String
)
