package mkn.pi.randomcoffee.entity.user

import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter(autoApply = true)
class UserStateConverter : AttributeConverter<UserState, String> {
    override fun convertToDatabaseColumn(state: UserState): String = state.name

    override fun convertToEntityAttribute(code: String): UserState = UserState.valueOf(code)
}
