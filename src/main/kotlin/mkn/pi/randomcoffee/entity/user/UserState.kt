package mkn.pi.randomcoffee.entity.user

enum class UserState {
    NOT_MATCHED,
    MATCHED,
    CANCELLED,
    MATCH_CANCELLED,
    NOTIFIED_ABOUT_CANCELLED_MATCH
}
