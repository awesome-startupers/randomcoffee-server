package mkn.pi.randomcoffee.entity.user

import mkn.pi.randomcoffee.entity.OrganizationEntity
import org.hibernate.Hibernate
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.MapsId


@Entity(name = "users")
data class UserEntity(
    @Id val id: Long,
    val nick: String = "",
    val name: String = "",
    val info: String = "",
    var wantParticipate: Boolean = false,
    var needConfirmation: Boolean = true,
    var state: UserState = UserState.NOT_MATCHED,

    val organizationId: Long
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as UserEntity

        return id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id )"
    }
}
