package mkn.pi.randomcoffee.component

import mkn.pi.randomcoffee.dto.ParticipationAnswerDto
import mkn.pi.randomcoffee.dto.RegistrationUserDto
import mkn.pi.randomcoffee.dto.UserDto
import mkn.pi.randomcoffee.entity.user.UserEntity
import mkn.pi.randomcoffee.entity.user.UserState
import mkn.pi.randomcoffee.repository.MatchRepository
import mkn.pi.randomcoffee.repository.OrganizationRepository
import mkn.pi.randomcoffee.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component
class UserComponent(
    @Autowired private val userRepository: UserRepository,
    @Autowired private val organizationRepository: OrganizationRepository,
    @Autowired private val matchRepository: MatchRepository
) {
    fun registerUser(user: RegistrationUserDto) {
        val organizationEntity = organizationRepository.findByKeyword(user.orgKeyword)
        if (organizationEntity.isEmpty) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Organization ${user.orgKeyword} doesn't exists")
        }
        if (organizationEntity.get().pincode != user.orgPincode) {
            throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Incorrect pincode")
        }
        userRepository.save(user.toEntity(organizationEntity.get().id!!))
    }

    fun userNotifiedAboutMatchCancellation(userId: Long) {
        val user = userRepository.findByIdOrNull(userId)!!
        user.state = UserState.NOTIFIED_ABOUT_CANCELLED_MATCH
        user.needConfirmation = true
        userRepository.save(user)
    }

    fun updateParticipationConfirm(userId: Long) {
        val user = userRepository.findByIdOrNull(userId)!!
        user.needConfirmation = false
        userRepository.save(user)
    }

    fun updateParticipationAnswer(userId: Long, participationAnswerDto: ParticipationAnswerDto) {
        val user = userRepository.findByIdOrNull(userId)!!
        user.wantParticipate = participationAnswerDto.wantsToParticipate
        userRepository.save(user)
    }

    fun getUsersNeedConfirmation(): List<UserDto> =
        userRepository.findAllByNeedConfirmation(true).map { it.toDto() }

    fun getUserById(userId: Long): UserDto = userRepository.findByIdOrNull(userId)!!.toDto()

    fun RegistrationUserDto.toEntity(organizationId: Long): UserEntity =
        UserEntity(id, nick, name, info, organizationId = organizationId)

    fun UserEntity.toDto(): UserDto = UserDto(id, nick, name, info, organizationId)

    fun getUsersWithCancelledMatches(): List<UserDto> =
        userRepository.findAllByState(UserState.MATCH_CANCELLED).map { it.toDto() }

    fun userCancelMatch(userId: Long) {
        val matchOptional = matchRepository.findByFirstUserIdOrSecondUserId(userId, userId)
        matchOptional.ifPresent {
            val match = matchOptional.get()
            val cancellingUser =
                if (userId == match.firstUserId) userRepository.findById(match.firstUserId).get()
                else userRepository.findById(match.secondUserId).get()
            val cancelledUser =
                if (userId != match.firstUserId) userRepository.findById(match.firstUserId).get()
                else userRepository.findById(match.secondUserId).get()
            cancellingUser.state = UserState.CANCELLED
            cancelledUser.state = UserState.MATCH_CANCELLED
            userRepository.save(cancellingUser)
            userRepository.save(cancelledUser)
        }
    }
}
