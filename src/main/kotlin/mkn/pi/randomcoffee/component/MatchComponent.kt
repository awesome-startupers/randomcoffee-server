package mkn.pi.randomcoffee.component

import mkn.pi.randomcoffee.dto.MatchDto
import mkn.pi.randomcoffee.dto.UserDto
import mkn.pi.randomcoffee.entity.MatchEntity
import mkn.pi.randomcoffee.repository.MatchRepository
import mkn.pi.randomcoffee.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MatchComponent(
    @Autowired private val matchRepository: MatchRepository,
    @Autowired private val userRepository: UserRepository,
    @Autowired private val userComponent: UserComponent
) {
    fun updateMatchNotified(matchId: Long) = deleteMatchById(matchId)

    fun deleteMatchById(matchId: Long) = matchRepository.deleteById(matchId)

    fun getMappingsNeedNotification(): List<MatchDto> = matchRepository.findAll()
        .filter { it.secondUserId != -1L }
        .map { it.toDto() }

    fun updateMissedNotified(usedId: Long) {
        matchRepository.deleteByFirstUserId(usedId)
        userRepository.save(userRepository.findById(usedId).get().apply { needConfirmation = true })
    }

    fun getUsersMissedInMatch(): List<UserDto> = matchRepository.findAll()
        .filter { it.secondUserId == -1L }
        .map { it.firstUserId }.map { userComponent.getUserById(it) }

    fun MatchEntity.toDto(): MatchDto = MatchDto(
        id!!,
        userComponent.getUserById(firstUserId),
        userComponent.getUserById(secondUserId)
    )
}
