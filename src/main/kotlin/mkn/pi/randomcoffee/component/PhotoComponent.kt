package mkn.pi.randomcoffee.component

import mkn.pi.randomcoffee.dto.PhotoDto
import mkn.pi.randomcoffee.entity.user.PhotoEntity
import mkn.pi.randomcoffee.repository.PhotoRepository
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class PhotoComponent(
    val photoRepository: PhotoRepository
) {
    fun addPhoto(photoDto: PhotoDto) {
        photoRepository.save(photoDto.toEntity())
    }

    fun getPhoto(userId: Long): PhotoDto = photoRepository
        .findByUserId(userId)
        .orElseGet { PhotoEntity(userId = userId, rawData = "") }
        .toDto()

    fun PhotoDto.toEntity() = PhotoEntity(userId = userId, rawData = photo)
    fun PhotoEntity.toDto() = PhotoDto(userId = userId, photo = rawData)
}
