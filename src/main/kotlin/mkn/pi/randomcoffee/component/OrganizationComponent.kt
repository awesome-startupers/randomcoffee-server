package mkn.pi.randomcoffee.component

import mkn.pi.randomcoffee.dto.OrganizationDto
import mkn.pi.randomcoffee.entity.OrganizationEntity
import mkn.pi.randomcoffee.repository.OrganizationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component
class OrganizationComponent(
    @Autowired val organizationRepository: OrganizationRepository,
) {
    fun addOrganization(organizationDto: OrganizationDto) {
        organizationRepository.save(organizationDto.toEntity())
    }

    fun getOrganizationById(organizationId: Long): OrganizationDto {
        val optionalOrganization = organizationRepository.findById(organizationId)
        if (optionalOrganization.isEmpty) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Organization with id $organizationId not found")
        }
        return optionalOrganization.get().toDto()
    }

    fun getAllOrganizations(): List<OrganizationDto> {
        return organizationRepository.findAll().map { it.toDto() }
    }

    fun patchOrganization(organizationDto: OrganizationDto) {
        organizationRepository.save(organizationDto.toEntity())
    }

    fun OrganizationDto.toEntity() =
        OrganizationEntity(name = this.name!!, keyword = this.keyword!!, pincode = this.pincode!!)

    fun OrganizationEntity.toDto() = OrganizationDto(id, name, keyword, pincode)
}
