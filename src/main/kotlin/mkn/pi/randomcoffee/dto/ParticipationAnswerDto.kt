package mkn.pi.randomcoffee.dto

data class ParticipationAnswerDto(val wantsToParticipate: Boolean)
