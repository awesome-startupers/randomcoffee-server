package mkn.pi.randomcoffee.dto

data class OrganizationDto(
    var id: Long?,
    val name: String,
    val keyword: String,
    val pincode: String,
)
