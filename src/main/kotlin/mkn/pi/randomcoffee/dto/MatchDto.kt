package mkn.pi.randomcoffee.dto

data class MatchDto(
    val id: Long,
    val user1: UserDto,
    val user2: UserDto
)
