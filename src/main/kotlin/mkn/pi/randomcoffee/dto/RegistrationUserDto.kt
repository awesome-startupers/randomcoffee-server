package mkn.pi.randomcoffee.dto

data class RegistrationUserDto (
    val id: Long,
    val nick: String,
    val name: String,
    val info: String,
    val orgKeyword: String,
    val orgPincode: String
)
