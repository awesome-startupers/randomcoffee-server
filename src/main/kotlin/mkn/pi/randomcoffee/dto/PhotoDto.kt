package mkn.pi.randomcoffee.dto

data class PhotoDto(
    val userId: Long,
    val photo: String
)
