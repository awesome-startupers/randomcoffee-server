package mkn.pi.randomcoffee.dto

data class UserDto(
    val id: Long,
    val nick: String,
    val name: String,
    val info: String,
    val orgId: Long
)
