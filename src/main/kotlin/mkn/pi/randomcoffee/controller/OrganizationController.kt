package mkn.pi.randomcoffee.controller

import mkn.pi.randomcoffee.component.OrganizationComponent
import mkn.pi.randomcoffee.dto.OrganizationDto
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("organization")
class OrganizationController(@Autowired val organizationComponent: OrganizationComponent) {

    private val logger = KotlinLogging.logger { }

    @PostMapping
    fun addOrganization(@RequestBody organization: OrganizationDto) =
        logger.info { "Request: POST /organization/" }
            .let { organizationComponent.addOrganization(organization) }
            .also { logger.info { "Response for POST /organization/: $it" } }


    @GetMapping("/{organizationId}")
    fun getOrganization(@PathVariable organizationId: Long): OrganizationDto =
        logger.info { "Request: GET /organization/$organizationId" }
            .let { organizationComponent.getOrganizationById(organizationId) }
            .also { logger.info { "Response for GET /organization/$organizationId: $it" } }


    @GetMapping
    fun getAllOrganizations(): List<OrganizationDto> =
        logger.info { "Request: GET /organization/" }
            .let { organizationComponent.getAllOrganizations() }
            .also { logger.info { "Response for GET /organization/: $it" } }

    @PatchMapping("/{organizationId}")
    fun patchOrganization(
        @PathVariable organizationId: Long,
        @RequestBody organizationDto: OrganizationDto,
    ) {
        logger.info { "Request: PATCH /organization/$organizationId" }

        if (organizationDto.id != null && organizationId != organizationDto.id) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        }

        organizationDto.id = organizationId

        return organizationComponent.patchOrganization(organizationDto)
            .also { logger.info { "Response for PATCH /organization/$organizationId: $it" }
        }
    }
}
