@file:Suppress("WildcardImport")

package mkn.pi.randomcoffee.controller

import mkn.pi.randomcoffee.component.MatchComponent
import mkn.pi.randomcoffee.dto.MatchDto
import mkn.pi.randomcoffee.dto.UserDto
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("match")
class MatchController(@Autowired private val matchComponent: MatchComponent) {

    private val logger = KotlinLogging.logger {}

    @PostMapping("/{matchId}/notified")
    fun updateMatchNotified(@PathVariable("matchId") matchId: Long) =
        logger.info { "Request: POST /match/$matchId/notified" }
            .let { matchComponent.updateMatchNotified(matchId) }
            .also { logger.info { "Response for POST /match/$matchId/notified: $it" } }

    @GetMapping("need-notification")
    fun getMatchingsNeedNotification(): List<MatchDto> =
        logger.info { "Request: GET /match/need-notification" }
            .let { matchComponent.getMappingsNeedNotification() }
            .also { logger.info { "Response for GET /match/need-notification: $it" } }

    @GetMapping("missed/need-notification")
    fun getMissedNeedNotification(): List<UserDto> =
        logger.info { "Request: GET /match/missed/need-notifiaction" }
            .let { matchComponent.getUsersMissedInMatch() }
            .also { logger.info { "Response for GET /match/missed/need-notification: $it" } }

    @PostMapping("missed/{userId}/notified")
    fun updateMissedNotified(@PathVariable("userId") userId: Long) =
        logger.info { "Request POST /match/missed/$userId/notified" }
            .let { matchComponent.updateMissedNotified(userId) }
            .also { logger.info { "Response for POST missed/match/$userId/notified: $it" } }
}
