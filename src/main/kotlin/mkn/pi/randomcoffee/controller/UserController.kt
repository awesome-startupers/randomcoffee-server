@file:Suppress("WildcardImport")

package mkn.pi.randomcoffee.controller

import mkn.pi.randomcoffee.component.UserComponent
import mkn.pi.randomcoffee.dto.ParticipationAnswerDto
import mkn.pi.randomcoffee.dto.RegistrationUserDto
import mkn.pi.randomcoffee.dto.UserDto
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("user")
class UserController(@Autowired private val userComponent: UserComponent) {

    private val logger = KotlinLogging.logger { }

    @PostMapping("/register")
    fun registerUser(@RequestBody user: RegistrationUserDto): Unit =
        logger.info { "Request: POST /user/register" }
            .let { userComponent.registerUser(user) }
            .also { logger.info { "Response for POST /user/register: $it" } }

    @PostMapping("/{userId}/participation/confirm")
    fun updateParticipationConfirm(@PathVariable("userId") userId: Long): Unit =
        logger.info { "Request: POST /user/$userId/participation/confirm" }
            .let { userComponent.updateParticipationConfirm(userId) }
            .also { logger.info { "Response for POST /user/$userId/participation/confirm: $it" } }

    @PostMapping("/{userId}/participation/answer")
    fun updateParticipationAnswer(
        @PathVariable("userId") userId: Long,
        @RequestBody participationAnswerDto: ParticipationAnswerDto,
    ): Unit =
        logger.info { "Request: POST /user/$userId/participation/answer" }
            .let { userComponent.updateParticipationAnswer(userId, participationAnswerDto) }
            .also { logger.info { "Response for POST /user/$userId/participation/answer: $it" } }

    @GetMapping("/participation/need-confirmation")
    fun getUsersNeedConfirmation(): List<UserDto> =
        logger.info { "Request: GET /participation/need-confirmation" }
            .let { userComponent.getUsersNeedConfirmation() }
            .also { logger.info { "Response for GET /participation/need-confirmation: $it" } }

    @PostMapping("/{userId}/match/cancellation/notified")
    fun userNotifiedAboutMatchCancellation(
        @PathVariable("userId") userId: Long,
    ): Unit = logger.info { "Request: POST /user/$userId/match/cancellation/notified" }
        .let { userComponent.userNotifiedAboutMatchCancellation(userId) }
        .also { logger.info { "Response for POST /user/$userId/match/cancellation/notified: $it" } }

    @GetMapping("/match/cancellation/need-notification")
    fun usersWithCancelledMatches(): List<UserDto> =
        logger.info { "Request: GET /user/match/cancellation/need-notification" }
            .let { userComponent.getUsersWithCancelledMatches() }
            .also { logger.info { "Response for GET /user/match/cancellation/need-notification: $it" } }

    @Suppress("UnusedPrivateMember")
    @GetMapping("/{userId}/match/cancellation")
    fun userCancelMatch(@PathVariable("userId") userId: Long): Unit = userComponent.userCancelMatch(userId)
}
