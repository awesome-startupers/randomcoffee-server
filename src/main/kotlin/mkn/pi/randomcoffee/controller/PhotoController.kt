package mkn.pi.randomcoffee.controller

import mkn.pi.randomcoffee.component.PhotoComponent
import mkn.pi.randomcoffee.dto.PhotoDto
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@RequestMapping("photo")
@RestController
class PhotoController(
    val photoComponent: PhotoComponent
) {
    @PostMapping("/")
    fun addPhoto(@RequestBody photo: PhotoDto) {
        photoComponent.addPhoto(photo)
    }

    @GetMapping("/{userId}")
    fun getPhoto(@PathVariable userId: Long): PhotoDto =
        photoComponent.getPhoto(userId)
}
