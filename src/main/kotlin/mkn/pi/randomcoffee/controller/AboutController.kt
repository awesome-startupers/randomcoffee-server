package mkn.pi.randomcoffee.controller

import mkn.pi.randomcoffee.properties.ConsulProperties
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("about")
class AboutController(
    private val consulProperties: ConsulProperties
) {
    @GetMapping("/")
    fun about(): String = consulProperties.about
}
