package mkn.pi.randomcoffee

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class RandomCoffeeApplication

fun main(args: Array<String>) {
	@Suppress("SpreadOperator")
	runApplication<RandomCoffeeApplication>(*args)
}
