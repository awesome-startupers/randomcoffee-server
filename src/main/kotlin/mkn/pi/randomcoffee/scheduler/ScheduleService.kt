package mkn.pi.randomcoffee.scheduler

import mkn.pi.randomcoffee.repository.MatchRepository
import mkn.pi.randomcoffee.repository.UserRepository
import mkn.pi.randomcoffee.entity.MatchEntity
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class ScheduleService(
    @Autowired val userRepository: UserRepository,
    @Autowired val matchRepository: MatchRepository
) {
    private val logger = KotlinLogging.logger { }

    fun doMatching() {
        logger.info { "New matching started" }
        val participantsGroups = userRepository.findAll()
            .filter { it.wantParticipate }
            .groupBy { it.organizationId }
        val toAsk = userRepository.findAll()
            .filter { !it.wantParticipate }
        matchRepository.deleteAll()

        for (pg in participantsGroups) {
            val participants = pg.value
            var i = 1
            while (i < participants.size) {
                val p1 = participants[i - 1]
                val p2 = participants[i]
                logger.info { "Users $p1 and $p2 matched" }
                matchRepository.save(
                    MatchEntity(
                        firstUserId = participants[i - 1].id,
                        secondUserId = participants[i].id
                    )
                )
                p1.wantParticipate = false
                p2.wantParticipate = false
                userRepository.save(p1)
                userRepository.save(p2)
                i += 2
            }
            if (i - 1 < participants.size) {
                val p1 = participants[i - 1]
                logger.info { "Users $p1 missed in match" }
                matchRepository.save(
                    MatchEntity(
                        firstUserId = p1.id,
                        secondUserId = -1L
                    )
                )
                p1.wantParticipate = false
                userRepository.save(p1)
            }
        }
        for (p in toAsk) {
            p.needConfirmation = true
            userRepository.save(p)
        }
        logger.info { "Matching finished" }
    }

    @Scheduled(fixedDelay = 30000) // 30 sec
    fun matching() {
        doMatching()
    }
}
