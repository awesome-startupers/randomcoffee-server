package mkn.pi.randomcoffee.health

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/health")
class HealthCheckController {
    @GetMapping("")
    @Suppress("FunctionOnlyReturningConstant")
    fun healthCheck(): Boolean {
        return true
    }
}
