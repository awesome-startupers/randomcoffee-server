package mkn.pi.randomcoffee.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Primary
@RefreshScope
@Configuration
@ConfigurationProperties("consul")
class ConsulProperties {
    var about: String = "Let's RandomCoffee!"
}
